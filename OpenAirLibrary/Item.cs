﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenAirLibrary
{
    public class Item
    {
        private static int NumOfID = 0;
        private int id;
        private string name;
        private float price;
        private int num;
        private string seller;
        private string buyer;
        private string intro;
        private int type;
        private int sellnum;
        private bool delete;

        public Item()
        {
            id = NumOfID++;
            name = "item";
            price = 0;
            num = 100;
            seller = "";
            buyer = "";
            intro = "";
            type = 0;
            sellnum = 0;
            delete = false;
        }

        public void SetName(string name)
        {
            this.name = name;
        }

        public void SetPrice(float p)
        {
            this.price = p;
        }

        public void SetNum(int n)
        {
            this.num = n;
        }

        public void SetSeller(string account)
        {
            this.seller = account;
        }

        public void SetBuyer(string account)
        {
            this.buyer = account;
        }

        public void SetIntro(string intro)
        {
            this.intro = intro;
        }

        public void SetType(int t)
        {
            this.type = t;
        }

        public void SetSellNum(int s)
        {
            this.sellnum = s;
        }

        public void SetDelete(bool b)
        {
            this.delete = b;
        }

        public string GetItemName()
        {
            return this.name;
        }

        public float GetPrice()
        {
            return this.price;
        }

        public int GetNum()
        {
            return this.num;
        }

        public string GetSeller()
        {
            return this.seller;
        }

        internal string GetBuyer()
        {
            return this.buyer;
        }

        public string GetIntro()
        {
            return this.intro;
        }

        public int GetType()
        {
            return this.type;
        }

        public int GetSellNum()
        {
            return this.sellnum;
        }

        public int GetID()
        {
            return this.id;
        }

        public int GetNumOfID()
        {
            return NumOfID;
        }

        public bool GetDelete()
        {
            return delete;
        }

    }
}
