﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenAirLibrary
{
    public class Orders
    {
        private string ID;
        private string itemName;
        private float price;
        private int quantity;
        private string buyer;
        private string seller;
        private string address;
        private int payChoose;
        private string[] payOption;

        static private List<Orders> orderData = new List<Orders>();

        public Orders()
        {
            ID = "";
            itemName = "item";
            price = 0;
            quantity = 0;
            buyer = "";
            seller = "";
            payChoose = -1;
            payOption = new string[] { "超商付款", "ATM轉帳", "7-11取貨" };
        }

        public int AddOrder(Orders order)
        {
            orderData.Add(order);

            return orderData.Count();
        }

        public int CheckQuantity(int exisitQuantity, int orderQuantity)
        {
            if (exisitQuantity > orderQuantity)
                return orderQuantity;

            return exisitQuantity;
        }

        public string CancelOrder(string id)
        {
            for(int i = 0; i < orderData.Count(); i++)
            {
                if ( ( orderData[i].GetOrderID() == id ) )
                {
                    orderData.Remove(orderData[i]);
                    return "訂單取消成功";
                }
            }

            return "無此訂單";
        }

        public void SetPayChoose(int choose)
        {
            this.payChoose = choose;
        }

        public string GetPayChoose()
        {
            return payOption[this.payChoose];
        }

        public string SearchOrder(string id)
        {
            for (int i = 0; i < orderData.Count(); i++)
            {
                if ((orderData[i].GetOrderID() == id))
                {
                    return "搜尋到此編號訂單";
                }
            }

            return "查無此訂單";
        }

        public string CheckOrderSuccess()
        {
            if (GetOrderQuantity() == 0)
            {
                CancelOrder(GetOrderID());
                return "Failed";
            }
            else
            {
                return "立即為您出貨";

            }
        }

        public void SetOrderID(string id)
        {
            this.ID = id;
        }

        public void SetOrderItemName(string name)
        {
            this.itemName = name;
        }

        public void SetOrderPrice(float totalprice)
        {
            this.price = totalprice;
        }

        public void SetOrderQuantity(int number)
        {
            this.quantity = number;
        }

        public void SetOrderBuyer(string buyername)
        {
            this.buyer = buyername;
        }

        public void SetOrderSeller(string sellername)
        {
            this.seller = sellername;
        }

        public void SetOrderAddress(string address)
        {
            this.address = address;
        }

        public string GetOrderID()
        {
            return this.ID;
        }

        public string GetOrderName()
        {
            return this.itemName;
        }

        public string GetOrderAddress()
        {
            return this.address;
        }

        public float GetOrderPrice()
        {
            return this.price;
        }

        public int GetOrderQuantity()
        {
            return this.quantity;
        }

        public string GetOrderBuyer()
        {
            return this.buyer;
        }

        public string GetOrderSeller()
        {
            return this.seller;
        }
    }
}
